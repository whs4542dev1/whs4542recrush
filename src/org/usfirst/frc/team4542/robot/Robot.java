package org.usfirst.frc.team4542.robot;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;

import com.ni.vision.NIVision;
import com.ni.vision.NIVision.Image;
import com.ni.vision.VisionException;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.RobotDrive.MotorType;
import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.TalonSRX;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Robot extends SampleRobot {
    private RobotDrive robotDrive;
    private Joystick mainStick  /*levers*/;
    private int session;
    private Image currentFrame;
    private TalonSRX liftMotor,conveyMotor;
    private Encoder liftEncoder, conveyEncoder;
    private DigitalInput liftLimit;
    private AnalogInput IR;
    private DriverStation ds;  
    
    //Throttle 
    private double throttle;
    private final double THROTTLE_MIN = .43;
    
    // variables to track internal lift
    private double liftGoal, liftReal, liftSpeed, oldSpeed,conveySpeed = 0.0;
    private double[] detents = { 0.0, 15.3, 46.7, 99.8 }; //TODO: get real detent values
    private final double detentInterval = 200;
    private final double LIFT_MAX=595;
    private final double LIFT_MIN=0;
    
    // Channels for the motors
    private final int frontLeftChannel	= 2;
    private final int rearLeftChannel	= 3;
    private final int frontRightChannel	= 1;
    private final int rearRightChannel	= 0;
    
    // The channel on the driver station that the joystick is connected to
    private final int joystickChannel = 0;
    private final int leversChannel = 1;
    
    //auton phases
    private enum Phase{STRAFE, LIFT, TURN, DRIVE, IDLE, LIFT2}
    boolean auto = true;
    int d1 =0;
    int d2 =500; 
    int d3 =4500;
    int d4 =750;
    int d5 =1500;
    
    public Robot() {
    	//drive
    	robotDrive = new RobotDrive(frontLeftChannel, rearLeftChannel, frontRightChannel, rearRightChannel);
        robotDrive.setExpiration(0.1);
    	
        robotDrive.setInvertedMotor(MotorType.kRearRight, false); //these do nothing but are included to make it easy to change if anything gets inadvertently reversed on the robot
    	robotDrive.setInvertedMotor(MotorType.kFrontRight, false);		
    	robotDrive.setInvertedMotor(MotorType.kFrontLeft, false);
    	robotDrive.setInvertedMotor(MotorType.kRearLeft, false);
        //control
    	mainStick = new Joystick(joystickChannel);
    	//levers = new Joystick(leversChannel);
    	
        //lift
        liftGoal = 0.0;
        liftReal = 0.0;//TODO: get from encoders
        liftMotor = new TalonSRX(4);
        liftEncoder = new Encoder(0,1,false);liftEncoder.setDistancePerPulse(.097);
        liftLimit = new DigitalInput(2);
        conveyMotor = new TalonSRX(5);
        conveyEncoder = new Encoder(3,4,false);
        IR = new AnalogInput(0);    
        
    }
    
    public void robotInit() {    	
    	
    	try {
    		currentFrame = NIVision.imaqCreateImage(NIVision.ImageType.IMAGE_RGB, 0);
    		session = NIVision.IMAQdxOpenCamera("cam1", NIVision.IMAQdxCameraControlMode.CameraControlModeController);
    		NIVision.IMAQdxConfigureGrab(session);
    		CameraServer.getInstance().setQuality(20);
    	} catch(VisionException e) {
    		System.out.println("camera not found");
    	}
    	
    	//put up values for auton times
    	SmartDashboard.putBoolean("Auto?", auto);
    	SmartDashboard.putNumber("strafe time", d2);
    	SmartDashboard.putNumber("lift time", d3);
    	SmartDashboard.putNumber("turn time", d4);
    	SmartDashboard.putNumber("drive time", d5);
    }
    
    public void operatorControl() {
        robotDrive.setSafetyEnabled(true);
        //NIVision.IMAQdxStartAcquisition(session);
        
        while (isOperatorControl() && isEnabled()) {
        	drive();
        	internalControls();
        	vision(); 
        	dashboard();
            Timer.delay(0.005);	// wait 5ms to avoid hogging CPU cycles
        }
        //NIVision.IMAQdxStopAcquisition(session);
    }
    
    public void disabled() {
    	auto = SmartDashboard.getBoolean("Auto?", auto);
    	d2=(int)SmartDashboard.getNumber("strafe time", d2);
    	d3=(int)SmartDashboard.getNumber("lift time", d3);
    	d4=(int)SmartDashboard.getNumber("turn time", d4);
    	d5=(int)SmartDashboard.getNumber("drive time", d5);
    }
    
    public void drive() {
    	//get inputs, modify appropriately (throttle from MIN to 1, deadzone on stick axes) 
    	throttle = ((-mainStick.getThrottle()+1)*.5)*(1-THROTTLE_MIN)+THROTTLE_MIN;
    	double jx = (Math.abs(mainStick.getX())>0.2)? mainStick.getX() : 0;
    	double jy = (Math.abs(mainStick.getY())>0.2)? mainStick.getY() : 0;
    	double jz = (Math.abs(mainStick.getZ())>0.2)? mainStick.getZ() : 0;
    	
    	//neutralize least axis to simplify control
    	if(Math.abs(jx) < Math.abs(jy) && Math.abs(jx) < Math.abs(jz)) jx = 0;
    	if(Math.abs(jy) < Math.abs(jx) && Math.abs(jy) < Math.abs(jz)) jy = 0;
    	if(Math.abs(jz) < Math.abs(jy) && Math.abs(jz) < Math.abs(jx)) jz = 0;
    	
    	robotDrive.mecanumDrive_Cartesian(jx*throttle, jy*throttle, jz*throttle, 0);
    }
    
    
    public void vision() {
    	try {
    		NIVision.IMAQdxGrab(session, currentFrame, 1);
    		CameraServer.getInstance().setImage(currentFrame);
    	} catch (VisionException ve){
    		System.out.println("Camera Lost: "+ve); 
    	}
    }
    
    boolean hasTote(){
    	return IR.getValue()>1;
    }
    
    public void dashboard() {
    	SmartDashboard.putNumber("J.X", mainStick.getRawAxis(0));
        SmartDashboard.putNumber("J.Y", mainStick.getRawAxis(1));
        SmartDashboard.putNumber("J.Z", mainStick.getRawAxis(2));
        SmartDashboard.putNumber("Throttle", throttle);
        //SmartDashboard.putNumber("L.1", levers.getRawAxis(0));
        //SmartDashboard.putNumber("L.2", levers.getRawAxis(1));
        //SmartDashboard.putNumber("L.3", levers.getRawAxis(2));
        SmartDashboard.putNumber("liftReal",liftReal);
        SmartDashboard.putNumber("liftGoal",liftGoal);
        SmartDashboard.putNumber("liftSpeed",liftSpeed);
        SmartDashboard.putNumber("Encoder",liftEncoder.getDistance());
        SmartDashboard.putNumber("tote",IR.getValue());
    }
    
    public void internalControls() {
    	//LIFT
    	//update sensors
    	if(liftLimit.get()) liftEncoder.reset();
    	liftReal = liftEncoder.getDistance();
    	//determine behavior
    	//manual by lever
    	/*
    	liftSpeed = -levers.getX()*Math.abs(levers.getX()); 
    	if (liftSpeed < 0.15 && liftSpeed > -0.15) { 
    		liftSpeed = 0;
    	} 
    	*/
    	liftSpeed=0;
    	//manual by main stick button
    	if (liftSpeed == 0){ 
    		if(mainStick.getRawButton(5)&&!mainStick.getRawButton(3)) liftSpeed = 1;
        	if(mainStick.getRawButton(3)&&!mainStick.getRawButton(5)) liftSpeed = -1; 		
    	}
    	//set goal based on manual speed
    	if (liftSpeed!=0) liftGoal = liftReal + liftSpeed*100; 	
    	if (liftSpeed == 0 && oldSpeed != 0) liftGoal = liftReal; 
    	//auto by button (lever or main)
    	if (liftSpeed == 0 && !mainStick.getRawButton(7) ){ //don't allow auto controls when manual controls are in use or safety override enabled 
    		if (liftGoal%detentInterval==0){
    			if (/*levers.getRawButton(1)||*/mainStick.getRawButton(6)) liftGoal = liftGoal+detentInterval;
	    		if (/*levers.getRawButton(2)||*/mainStick.getRawButton(4)) liftGoal = liftGoal-detentInterval; 
    		}else{
    			if (/*levers.getRawButton(1)||*/mainStick.getRawButton(6)) liftGoal = liftGoal+detentInterval-liftGoal%detentInterval;
	    		if (/*levers.getRawButton(2)||*/mainStick.getRawButton(4)) liftGoal = liftGoal-liftGoal%detentInterval;
    		}
    	}
    	//move based on goal
    	//if (liftGoal < LIFT_MIN && !mainStick.getRawButton(7)) liftGoal = LIFT_MIN;
    	if (liftGoal > LIFT_MAX && !mainStick.getRawButton(7)) liftGoal = LIFT_MAX;
    	oldSpeed = liftSpeed;
    	if(Math.abs(liftGoal-liftReal)>5){
    		liftMotor.set((Math.abs(liftGoal-liftReal))>1 ? 1*Math.signum(liftGoal-liftReal) : (liftGoal-liftReal)); //talon won't accept value >1 so we have to max this ourselves
    	}else{
    		liftMotor.set(0);
    	}
    	//CONVEYOR
    	conveySpeed=0;   	
    	if(mainStick.getRawButton(1)&&!mainStick.getRawButton(2)) conveySpeed=1;
    	if(mainStick.getRawButton(2)&&!mainStick.getRawButton(1)) conveySpeed=-1;   	
    	conveyMotor.set(conveySpeed);
    }
    
    
	public void autonomous() {
		long t = 0;
		long lastLoop = System.currentTimeMillis();
		Phase p = Phase.IDLE;
		int t1 = 0;//start
		int t2 = t1 + d2;// stop strafe start lift
		int t3 = t2 + d3;// lift -> turn
		int t4 = t3 + d4;// turn -> drive
		int t5 = t4 + d5;// drive ->lift2
		int t6 = t5 + d3;// end
		while (isAutonomous() && isEnabled())
		{ 
			if(auto){
				if(t<t1+100) p = Phase.STRAFE;
				if(t>t2 && t<t2+100) p = Phase.LIFT;
				if(t>t3 && t<t3+100) p = Phase.TURN;
				if(t>t4 && t<t4+100) p = Phase.DRIVE;
				if(t>t5 && t<t5+100) p = Phase.LIFT2;
				if(t>t6) p = Phase.IDLE;
				
				if(p==Phase.LIFT||p==Phase.LIFT2){ 
					if(p==Phase.LIFT) liftMotor.set(1);
					if(p==Phase.LIFT2) liftMotor.set(-1); 
				}else{ liftMotor.set(0); }
				if(p==Phase.TURN||p==Phase.DRIVE||p==Phase.STRAFE){ 
					if(p==Phase.STRAFE) robotDrive.mecanumDrive_Cartesian(0.3,0,0,0);
					if(p==Phase.TURN) robotDrive.mecanumDrive_Cartesian(0,0,0.5,0);
					if(p==Phase.DRIVE) robotDrive.mecanumDrive_Cartesian(0,0.5,0,0);
				}else{ robotDrive.mecanumDrive_Cartesian(0,0,0,0); }
				
				t+= System.currentTimeMillis()-lastLoop; //inc by 1/60 of a second to approximate the time passage of a single cycle. every other solution is much too heavy duty for this problem. accuracy isn't critical
				lastLoop = System.currentTimeMillis();
				System.out.println(p);
				System.out.println(t);
			}	
		}
	}
}


